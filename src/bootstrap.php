<?php

namespace Lkt\Cache;

use Lkt\Cache\Console\Commands\CachePurgeCommand;
use Lkt\Commander\Commander;

if (php_sapi_name() === 'cli') {
    Commander::register(new CachePurgeCommand());
}