<?php

namespace Lkt\Cache\Console\Commands;

use Lkt\Cache\Settings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CachePurgeCommand extends Command
{
    protected static $defaultName = 'lkt:cache:purge';

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $path = Settings::getStorePath();
        if (!$path) {
            $output->writeln("There isn't an store path configured yet!");
            $output->writeln("Aborting");
            return 1;
        }

        $files = glob($path . '/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                $state = unlink($file);
                if (!$state) {
                    $output->writeln("Unable to delete {$file}");
                }
            } elseif (is_dir($file)) {
                $this->removeDir($file, $output);
            }
        }

        $output->writeln("Done!");
        return 1;
    }

    /**
     * @param $dir
     * @param OutputInterface $output
     * @return void
     */
    protected function removeDir($dir, OutputInterface $output)
    {
        if (is_dir($dir)) {
            $output->writeln("Directory: {$dir}");
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..') {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                        $this->removeDir($dir . DIRECTORY_SEPARATOR . $object, $output);
                    } else {
                        $state = unlink($dir . DIRECTORY_SEPARATOR . $object);
                        if (!$state){
                            $output->writeln("Unable to delete {$object}");
                        }
                    }
                }
            }
            rmdir($dir);
        }
    }
}