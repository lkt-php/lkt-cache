<?php

namespace Lkt\Cache;

use Carbon\Carbon;
use Cocur\Slugify\Slugify;


class Content
{
    const MODIFIED_STAMP_CODES_CODE = 'lkt-control-modified-stamp-codes';
    const FORCE_REFRESH_CODES_CODE = 'lkt-control-force-refresh-codes';

    const SKIP_CODES = [
        'lkt-control-modified-stamp-codes',
        'lkt-control-force-refresh-codes',
    ];

    protected static $STAMP_MAP = [];
    protected static $FORCE_REFRESH_MAP = [];
    protected static $LOADED = false;

    protected static function autoload()
    {
        if (self::$LOADED === true) {
            return;
        }

        self::$STAMP_MAP = json_decode(self::load(self::MODIFIED_STAMP_CODES_CODE), true);
        self::$FORCE_REFRESH_MAP = json_decode(self::load(self::FORCE_REFRESH_CODES_CODE), true);
        self::$LOADED = true;
    }

    /**
     * @param string $code
     * @return string
     */
    protected static function parseCode(string $code): string
    {
        $slug = new Slugify();
        return $slug->slugify($code);
    }

    /**
     * @param string $parsedCode
     * @return string
     */
    protected static function getPathToParsedCode(string $parsedCode): string
    {
        $path = Settings::getStorePath();
        return "{$path}/{$parsedCode}.bak";
    }

    /**
     * @param string $code
     * @param string $content
     */
    public static function store(string $code, string $content = '')
    {
        $codeSlug = self::parseCode($code);
        $fullPath = self::getPathToParsedCode($codeSlug);
        if (!in_array($codeSlug, self::SKIP_CODES)) {
            self::autoload();
        }

        if (file_put_contents($fullPath, $content)) {
            if (isset(self::$FORCE_REFRESH_MAP[$codeSlug])) {
                unset(self::$FORCE_REFRESH_MAP[$codeSlug]);
                self::store(self::FORCE_REFRESH_CODES_CODE, json_encode(self::$FORCE_REFRESH_MAP));
            }

            if (!in_array($codeSlug, self::SKIP_CODES)) {
                $modifiedDate = date('Y-m-d H:i:s', filemtime($fullPath));
                self::$STAMP_MAP[$codeSlug] = $modifiedDate;
                self::store(self::MODIFIED_STAMP_CODES_CODE, json_encode(self::$STAMP_MAP));
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $code
     * @return Carbon
     * @throws \Exception
     */
    public static function stamp(string $code)
    {
        $codeSlug = self::parseCode($code);
        if (isset(self::$STAMP_MAP[$code])) {
            return new Carbon(self::$STAMP_MAP[$code]);
        }

        $fullPath = self::getPathToParsedCode($codeSlug);
        if (file_exists($fullPath)) {
            $modifiedDate = date('Y-m-d H:i:s', filemtime($fullPath));
            self::$STAMP_MAP[$codeSlug] = $modifiedDate;
            return new Carbon($modifiedDate);
        }

        return false;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function load(string $code): string
    {
        $codeSlug = self::parseCode($code);
        if (!in_array($codeSlug, self::SKIP_CODES)) {
            self::autoload();
        }
        if (in_array($codeSlug, self::SKIP_CODES) || isset(self::$STAMP_MAP[$codeSlug])) {
            $fullPath = self::getPathToParsedCode($codeSlug);
            if (file_exists($fullPath)) {
                return file_get_contents($fullPath);
            }
        }
        return '';
    }

    /**
     * @param string $code
     * @param int $maxTimeAliveInSeconds
     * @return bool
     * @throws \Exception
     */
    public static function expired(string $code, int $maxTimeAliveInSeconds): bool
    {
        $codeSlug = self::parseCode($code);
        self::autoload();
        $stamp = self::stamp($code);
        if (!is_object($stamp)) {
            return true;
        }

        if (isset(self::$FORCE_REFRESH_MAP[$codeSlug])) {
            return true;
        }

        if ($maxTimeAliveInSeconds === 0) {
            return true;
        }

        if ($maxTimeAliveInSeconds < 0) {
            return false;
        }

        if (is_object($stamp)) {
            $now = new Carbon();
            $stamp->addSeconds($maxTimeAliveInSeconds);
            return !$stamp->isAfter($now);
        }
        return true;
    }

    /**
     * @param string $code
     */
    public static function forceRefresh(string $code)
    {
        $codeSlug = self::parseCode($code);
        self::$FORCE_REFRESH_MAP[$codeSlug] = true;
        self::store(self::FORCE_REFRESH_CODES_CODE, json_encode(self::$FORCE_REFRESH_MAP));
    }

    /**
     * @param string $code
     * @return bool
     * @throws \Exception
     */
    public static function drop(string $code) :bool
    {
        $codeSlug = self::parseCode($code);
        self::autoload();
        $stamp = self::stamp($code);
        if (!is_object($stamp)) {
            return true;
        }
        $fullPath = self::getPathToParsedCode($codeSlug);
        if (file_exists($fullPath)) {
            return unlink($fullPath);
        }
        return true;
    }
}