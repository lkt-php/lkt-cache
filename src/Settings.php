<?php

namespace Lkt\Cache;

use Lkt\InstancePatterns\Traits\SingleTonTrait;
use Lkt\Locale\Locale;


class Settings
{
    use SingleTonTrait;

    protected $storePath;
    protected $locale;

    /**
     * @param string $path
     */
    public static function setStorePath(string $path = '')
    {
        self::getInstance()->storePath = $path;
    }

    /**
     * @return string
     */
    public static function getStorePath()
    {
        $base = trim(rtrim(self::getInstance()->storePath, '/'));
        $locale = trim(rtrim(Locale::getLangCode()));
        if ($locale !== '') {
            $r = "{$base}/{$locale}";
            if (!is_dir($r)) {
                mkdir($r, 2777);
            }
            return $r;
        }
        return $base;
    }
}